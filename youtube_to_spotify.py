"""
Log into Youtube
Get our liked videos
Log into spotify
Create a new playlist
Search for song
Add song to playlist
"""
import json
import os

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
import requests
import youtube_dl

from secret import spotify_token, spotify_user_id
# youtube_dl

class CreatePlayList:

    def __init__(self):
        self.youtube_client = self.get_youtube_client()
        self.all_song_info = {}
        self.youtube_liked_videos =self.get_liked_videos()

        

    #Log into Youtube account
    def get_youtube_client(self):
        # Disable OAuthlib's HTTPS verification when running locally.
        # *DO NOT* leave this option enabled in production.
        os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

        api_service_name = "youtube"
        api_version = "v3"
        client_secrets_file = "client_secret.json"

        # Get credentials and create an API client
        scopes = ["https://www.googleapis.com/auth/youtube.readonly"]
        flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
            client_secrets_file, scopes)
            
        print("getting credentials\n")
        credentials = flow.run_console()
        print("done gettting credentials\n")

        youtube_client = googleapiclient.discovery.build(
            api_service_name, api_version, credentials=credentials)

        return youtube_client

    #Get liked videos
    def get_liked_videos(self):
        """Grab Our Liked Videos & Create A Dictionary Of Important Song Information"""
        request = self.youtube_client.videos().list(
            part="snippet,contentDetails,statistics",
            myRating="like"
        )
        response = request.execute()

        ydl_ops = {
            "nocheckcertificate": True
        }
        for item in response["items"]:
            video_title = item["snippet"]["title"]
            youtube_url = "https://www.youtube.com/watch?v={}".format(
                item["id"])

            # use youtube_dl to collect the song name & artist name
            video = youtube_dl.YoutubeDL(ydl_ops).extract_info(
                youtube_url, download=False)
            song_name = video["track"]
            artist = video["artist"]

            if song_name is not None and artist is not None:
                self.all_song_info[video_title] = {
                    "youtube_url": youtube_url,
                    "song_name": song_name,
                    "artist": artist
                }

        print(self.all_song_info)
            
        return self.all_song_info


    #Create new playlist
    def create_playlist(self):
        
        request_body = json.dumps({
            "name": "Youtube Liked Videos",
            "description": "This is a playlist of my liked videos from Youtube",
            "public": True
        })

        query = "https://api.spotify.com/v1/users/{}/playlists".format(
            spotify_user_id
        )

        response = requests.post(
            query,
            data=request_body,
            headers={
                "Content-Type": "application/json",
                "Authorization": "Bearer {}".format(spotify_token)
            }
        )

        if response.status_code != 201:
            print("something went wrong")
            print(response.status_code)
            return -1
        else:
            response_json = response.json()
            print("playlist succesfully created: ", response_json)
            return response_json["id"]

    #Search song
    def get_spottify_uri():
        pass

    #Add song to playlist
    def add_song_to_playlist():
        pass

if __name__ == '__main__':
    cp = CreatePlayList()
    